# Proceso Front 201807

Hola!

La idea de esta prueba es remodelar el formulario actual de alta de anuncio para
que sea responsive.

En la raíz del proyecto hay un pdf que tiene el diseño pasado por el equipo de
producto. Generalmente, somos bastantes precisos en cuanto a distancias y píxeles
pero siendo una prueba orientativa y un pdf el formato del diseño, lo que
buscamos es más ver cómo te desenvuelves, intentanto siempre ser lo más preciso
posible, claro.

Para que te sea más fácil ubicarte, el código que tienes que hacer deberá estar
en la carpeta sass/newStructure de la raíz. Ahí encontrarás una adaptación de la
arquitectura SMACSS que estamos aplicando a los proyectos.
El orden de las reglas de scss es concéntrica.
Ya hay código creado, por lo que es posible que te sirva de ayuda para
algunas cosas ;)

En index.html es el html que podrías tocar para conseguir la maquetación pedida.

Tienes 1,5 horas para realizar la prueba desde el momento en que te llegue. No
queremos que gastes más tiempo, así que hasta donde hayas llegado con una hora
nos basta para conocerte, y te bastará para ver si se sientes cómodo con la
arquitectura.

Créate una branch de master, para cuando acabes puedas hacernos una pull request

Mucha suerte!