/**
 * Indica si es una tecla numérica (tiene en cuenta el panel numérico)
 * @param int data tecla pulsada
 * @returns true si es numércio, false si no.
 */
function isAscciiNumber(data) {
    return (data > 47 && data < 58) || (data > 95 && data < 106);
}

/**
 * Indica si es una tecla especial de la siguiente lista
 * 8  BACKSPACE
 * 9  TAB
 * 13 NEW LINE
 * 27 ESC
 * 35 END
 * 36 START
 * 37 Left Arrow
 * 38 Up Arrow
 * 39 Right Arrow
 * 40 Down Arrow
 * 46 DEL
 * @param int data tecla pulsada
 * @returns boolean TRUE si es un caracter de la lista
 */
function isSpecialAsccii(data) {
    return (
        data == 8  ||
        data == 9  ||
        data == 13 ||
        data == 27 ||
        data == 35 ||
        data == 36 ||
        data == 37 ||
        data == 38 ||
        data == 39 ||
        data == 40 ||
        data == 46
    );
}


/**
 * Comprueba que todos los inputs de los selectores no estén vacíos
 * @param array data
 * @returns {Boolean}
 */
function checkAllInputsNotEmpty(selector) {
    var ok = true;
    $(selector).each(function () {
        if ($(this).val() === "") {
            ok = false;
        }
    });
    return ok;
}

/**
 * Función que valida un formulario, si el primero de sus campos, marcado como "selector" está
 * vacío, entonces lanza el evento "event"
 * @param string selector
 * @param string event
 *
 * @return boolean true si todo bien, false si alguno está vacío
 */
function checkRequireParams(selector, event) {
    event = (typeof event !== 'undefined') ? event : "blur";
    var result = true;
    $(selector).each(function () {
        var element = $("#" + $(this).attr("for"));
        if ($(this).hasClass('input-type-radio')) {
            if (result) {
                element.trigger(event);
                if (!element.find('input:radio').is(':checked')) {
                    result = false;
                }
            }
        } else if (element.val() === "") {
            if (result) {
                addErrorMessage(element, "El campo es obligatorio");
                element.parent().removeClass('success');
                element.trigger(event);
                result = false;
            }
        }
    });
    return result;
}

function addErrorMessage(element, message) {
    var attrid = (element.attr('id') != 'undefined') ? "id='" + element.attr('id') + "_em_'" : "";
    element.parent().addClass("error");
    if (element.parent().find(".errorMessage").length === 0) {
        var e = $("<div class='errorMessage'" + attrid + "'>" + message + "</div>");
        element.parent().append(e);
        element.parent().removeClass('success');
    } else {
        if (!element.parent().find(".errorMessage").is(":visible")) {
            element.parent().find(".errorMessage").detach();
            var e = $("<div class='errorMessage' " + attrid + ">" + message + "</div>");
            element.parent().append(e);
        }
    }
}

function removeErrorMessage(element) {

    element.parent().removeClass("error");
    element.parent().find(".errorMessage").hide();
}


/**
 * Comprueba que no exista ningún div con la clase error
 * @param {type} parentSelector
 * @param {type} sonsSelector
 * @returns {Boolean}
 */
function checkErrorParams(parentSelector, sonsSelector) {
    return ($(parentSelector).find(sonsSelector).length == 0);
}


/**
 *
 * @param {type} id
 * @returns {undefined}
 */
function resetSelect(selector) {
    if ($(selector).val() !== "") {
        $(selector).html('<option value="">Selecciona</option>');
    }
}

function removeCSSClass(selector, cssClass, parent) {
    var element;
    if (!parent) {
        element = $(selector);
    } else {
        element = $(selector).parent();
    }
    element.removeClass(cssClass);
}


function resetSelectsYiiForm(selectors) {
    resetSelect(selectors);
    removeCSSClass(selectors, "success", true);
    removeCSSClass(selectors, "error", true);
}

function updateSelectsFormYii(elementTag, data, prompt) {
    if (typeof(prompt) === 'undefined') prompt = 'Selecciona';
    var nextTag = (typeof $(elementTag).attr("data-next") !== 'undefined') ? $(elementTag).attr("data-next") : "";
    $(nextTag).html(data);
    removeErrorMessage($(nextTag));
    if (nextTag !== "") {
        updateSelectsFormYii(nextTag, '<option value="">'+ prompt +'</option>');
        $(nextTag).parent().removeClass('success');
    }
}


function checkNotURL(val) {
    var regexp = new RegExp([
        '(', '\\s|[^a-zA-Z0-9.\\+_\\/"\\>\\-]|^', ')(?:', '(', '[a-zA-Z0-9\\+_\\-]+',
        '(?:', '\\.[a-zA-Z0-9\\+_\\-]+', ')*@', ')?(', 'http:\\/\\/|https:\\/\\/|ftp:\\/\\/', ')?(',
        '(?:(?:[a-z0-9][a-z0-9_%\\-_+]*\\.)+)', ')(',
        '(?:com|ca|co|edu|gov|net|org|dev|biz|cat|int|pro|tel|mil|aero|asia|coop|info|jobs|mobi|museum|name|post|travel|local|[a-z]{2})',
        ')(', '(?::\\d{1,5})', ')?(', '(?:', '[\\/|\\?]', '(?:', '[\\-a-zA-Z0-9_%#*&+=~!?,;:.\\/]*',
        ')*', ')', '[\\-\\/a-zA-Z0-9_%#*&+=~]', '|', '\\/?', ')?',
        ')(', '[^a-zA-Z0-9\\+_\\/"\\<\\-]|$', ')'
    ].join(''), 'g');
    var data = val.replace(/ /g, " \n ").match(regexp);
    var error = false;
    if (data !== null) {
        data.forEach(function (entry) {
            if (!entry.match(/coches.com/) && !entry.match(/.*@.*/)) {
                error = true;
            }
        });
    }
    return !error;
}

//noinspection JSUnusedGlobalSymbols
/**
 * Desactiva el paste en el input recuperado por el id
 */
function preventOnPaste(id) {
    window.onload = function () {
        var myInput = document.getElementById(id);
        myInput.onpaste = function (e) {
            e.preventDefault();
        }
    }
}


$(document).ready(function () {

    $(".only-number").keydown(function (data) {
        if (229 == data.keyCode) return;
        if (isAscciiNumber(data.keyCode) || isSpecialAsccii(data.keyCode) || (data.ctrlKey && data.keyCode==86)) {
            removeErrorMessage($(this));
            return true;
        } else {
            addErrorMessage($(this), "Solo se pueden escribir números.");
            return false;
        }
    });

    $(".only-number").keyup(function (data) {
        if (229 != data.keyCode) return;
        var t = /[^0-9]+/, v = $(this).val();
        if ( t.test(v)) {
            v = v.replace(/[^0-9]+/,'');
            $(this).val(v);
            addErrorMessage($(this), "Solo se pueden escribir números.");
        } else {
            removeErrorMessage($(this));
        }
    });

    $(".no-url").keydown(function (data) {
        if (!checkNotURL($(this).val())) {
            addErrorMessage($(this), "Los comentarios no pueden contener una dirección web.");
        } else {
            removeErrorMessage($(this));
        }
    });

    $(".cascadeSelect").change(function () {
        if ($(this).val() === "") {
            addErrorMessage($(this), "El campo es obligatorio");
            $(this).parent().removeClass('success');
        } else {
            removeErrorMessage($(this));
        }
    });

    $(".checkFormParams").click(function () {
        var formTag = $(this).attr('data-form');
        if (!checkRequireParams(formTag + " .required") || !checkErrorParams(formTag, ".error"))
            return false;
    });


    $("form div.row div.infoMessage").parent().click(function () {
        $(this).addClass("info");
    });

    $("form div.row textarea, form div.row input, form div.row select").blur(function () {
        $(this).parent().removeClass("info");
    });

    $('form select').change(function () {
        if ($(this).val() != '') {
            $(this).parent().removeClass('active').addClass('success');
            $(this).parent().next('.select').addClass('active');
        } else {
            $(this).parent().parent().find('.active').removeClass('active');
            $(this).parent().addClass('active');
            $(this).parent().removeClass('success');
        }
    });

    $(".scrollTopButton").click(function () {
        $('html,body').animate({scrollTop: 0}, 1000);
    });

    $(".countChars").keyup(function (data) {
        $($(this).attr("data-text-countchars")).text($(this).val().length);
    });

});